import { defineComponent } from 'vue';

const LocalValueMixin = defineComponent({
  props: {
    modelValue: {
      type: undefined,
      required: true,
    },
  },
  computed: {
    localValue: {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      get(): any {
        return this.modelValue;
      },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      set(value: any): void {
        this.$emit('update:modelValue', value);
      },
    },
  },
});

export default LocalValueMixin;
